var showingBurgerMenu = false;

// When the user scrolls the page, execute
window.onscroll = function() {displayPageProgress()};

function displayPageProgress() {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    document.getElementById("progress-indicator").style.width = scrolled + "%";
}

function showMenu() {
    showingBurgerMenu = !showingBurgerMenu;

    var mobileMenu = document.getElementById("burger-menu");

    if (showingBurgerMenu) {
        mobileMenu.classList.add("show");
    } else {
        mobileMenu.classList.remove("show");
    }
}

function copyText(buttonDiv) {
    for (var i=0;i<buttonDiv.parentNode.childNodes.length;i++) {

        if (buttonDiv.parentNode.childNodes[i].tagName!=undefined) {
            if (buttonDiv.parentNode.childNodes[i].tagName.toLowerCase()=="code") {
                var codeBlock = buttonDiv.parentNode.childNodes[i];
                var copyText = codeBlock.innerHTML;
                const textArea = document.createElement('textarea');
                textArea.textContent = copyText;
                document.body.append(textArea);
                textArea.select();
                document.execCommand("copy");
            }
        }
    }
}

function showCopyButton(preDiv) {
    for (var i=0;i<preDiv.childNodes.length;i++) {

        if (preDiv.childNodes[i].tagName!=undefined) {
            if (preDiv.childNodes[i].tagName.toLowerCase()=="button") {
                preDiv.childNodes[i].classList.add("show");
            }
        }
    }
}

function hideCopyButton(preDiv) {
    for (var i=0;i<preDiv.childNodes.length;i++) {

        if (preDiv.childNodes[i].tagName!=undefined) {
            if (preDiv.childNodes[i].tagName.toLowerCase()=="button") {
                preDiv.childNodes[i].classList.remove("show");
            }
        }
    }
}

function showKotlinCode() {
    var arr = document.getElementsByClassName("java");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.remove("selected");
    }

    var arr = document.getElementsByClassName("kotlin");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.add("selected");
    }

    var arr = document.getElementsByClassName("showKotlin");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.remove("deselected");
        arr[i].classList.add("selected");
    }

    var arr = document.getElementsByClassName("showJava");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.remove("selected");
        arr[i].classList.add("deselected");
    }
}

function showJavaCode() {
    var arr = document.getElementsByClassName("kotlin");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.remove("selected");
    }

    var arr = document.getElementsByClassName("java");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.add("selected");
    }

    var arr = document.getElementsByClassName("showKotlin");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.remove("selected");
        arr[i].classList.add("deselected");
    }

    var arr = document.getElementsByClassName("showJava");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.remove("deselected");
        arr[i].classList.add("selected");
    }
}

function showSwiftCode() {
    var arr = document.getElementsByClassName("objc");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.remove("selected");
    }

    var arr = document.getElementsByClassName("swift");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.add("selected");
    }

    var arr = document.getElementsByClassName("showSwift");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.remove("deselected");
        arr[i].classList.add("selected");
    }

    var arr = document.getElementsByClassName("showObjC");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.remove("selected");
        arr[i].classList.add("deselected");
    }
}

function showObjCCode() {
    var arr = document.getElementsByClassName("swift");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.remove("selected");
    }

    var arr = document.getElementsByClassName("objc");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.add("selected");
    }

    var arr = document.getElementsByClassName("showSwift");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.add("deselected");
        arr[i].classList.remove("selected");
    }

    var arr = document.getElementsByClassName("showObjC");
    for (var i=0;i<arr.length;i++) {
        arr[i].classList.add("selected");
        arr[i].classList.remove("deselected");
    }
}
