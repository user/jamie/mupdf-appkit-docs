# Bitmap Exporting

<div class="banner">
    <div class="ios-text"></div>
    <div class="vendor-logo ios single"></div>
</div>

##  

An application developer can export a document's pages into bitmap format by invoking the `MuPDF` library directly, opening the document and processing the resulting pages into a bitmap file sequence.

## Loading MuPDF without a UI

To load the `MuPDF` library and use it directly an application developer should import `mupdfdk` and request a document load as follows:

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">


<div class="swift selected">



```
import mupdfdk

var mupdfdkLib:MuPDFDKLib?

func processDocument(_ docPath:String) {
    let settings:ARDKSettings = ARDKSettings()
    mupdfdkLib = MuPDFDKLib.init(settings: settings)

    let fullPath:String = "\(FileManager.default.urls(for: .documentDirectory,
                          in: .userDomainMask).last?.path ?? "")/\(docPath)"

    let doc:MuPDFDKDoc = mupdfdkLib.doc(forPath: fullPath,
                    of: MuPDFDKDoc.docType(fromFileExtension: docPath)) as! MuPDFDKDoc

    doc.successBlock = {
        // See iOS Sample App
    }

    doc.errorBlock = {(error:ARDKDocErrorType?) in

    }

    doc.loadDocument()

}
```

</div>

<div class="objc">


```
#import <mupdfdk/mupdfdk.h>

@property (strong, nonatomic) MuPDFDKLib *mupdfdkLib;

- (void)processDocument:(NSString *)docPath {
    ARDKSettings *settings = [[ARDKSettings alloc] init];
    self.mupdfdkLib = [[MuPDFDKLib alloc] initWithSettings:settings];

    MuPDFDKDoc *doc = [_mupdfdkLib docForPath:[[self nsDocumentsDirectory].path
        stringByAppendingPathComponent:docPath]
    ofType:[MuPDFDKDoc docTypeFromFileExtension:docPath]];

    doc.successBlock = ^() {
        // See iOS Sample App
    };

    doc.errorBlock = ^(ARDKDocErrorType error) {
        NSLog(@"Failed to load document: %x", error);
    };

    [doc loadDocument];
}

- (NSURL *)nsDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
              inDomains:NSUserDomainMask] lastObject];
}
```

</div>

</div>


Upon document load completion an application developer should create an asynchronous task in the `successBlock` to render pages as bitmaps using the `bitmapAtSize` method of `MuPDFDKDoc` (the document instance) against the document's `MuPDFDKPage` page instances. See the [iOS Sample app] and the `ConvertToPNGSample` folder reference for a full example.


[iOS Sample app]: download.html#ios
