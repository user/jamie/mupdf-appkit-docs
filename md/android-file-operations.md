# File Operations

<div class="banner">
    <div class="android-text"></div>
    <div class="vendor-logo android single"></div>
</div>



##  

There are a number of file operations available against a document, an application developer should only be required to implement these if using the [Custom UI].

### Save

Saving a document only needs to be invoked if there are changes made to a document. As such an application developer can verify if changes have been made or not and act accordingly.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
if (mDocumentView.isDocumentModified()) {
    mDocumentView.save()
}
```

</div>

<div class="java">



```
if (mDocumentView.isDocumentModified()) {
    mDocumentView.save();
}
```

</div>

</div>


### Save As

When saving a document using this method an application developer must provide a valid path on the file system to save to.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
val docPath:String = "<YOUR_DOCUMENT_PATH>"

mDocumentView.saveTo(docPath) { result, err ->
    if (result == SODocSaveListener.SODocSave_Succeeded) { // success

    } else { //  error

    }
}
```

</div>

<div class="java">



```
String docPath = "<YOUR_DOCUMENT_PATH>";

mDocumentView.saveTo(newPath, new SODocSaveListener() {
    @Override
    public void onComplete(int result, int err) {
        if (result == SODocSave_Succeeded) { //  success

        } else { //  error

        }
    }
});
```

</div>

</div>


### Print

Application developers should call the `print()` method against the `DocumentView` instance to open up the print dialog.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
mDocumentView.print()
```

</div>

<div class="java">



```
mDocumentView.print();
```

</div>

</div>


### Search

Searching is invoked from the current document selection or cursor position and can be made forward or backward from this point. Successful searching automatically highlights the next instance of a found `String` and moves the document selection to that point.


> **NOTE**  
> Search is case-insensitive.
>


<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
fun search(text:String, Boolean:forward) {
    if (forward) {
        mDocumentView.searchForward(text)
    } else {
        mDocumentView.searchBackward(text)
    }
}
```

</div>

<div class="java">



```
private void search(String text, forward Boolean) {
    if (forward) {
        mDocumentView.searchForward(text);
    } else {
        mDocumentView.searchBackward(text);
    }
}
```

</div>

</div>



### Get selected text

To get the selected text from a document an application developer should request the `selectedText` property against the `DocumentView` instance.


<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
val selectedText:String? = mDocumentView?.selectedText
```

</div>

<div class="java">



```
String selectedText = mDocumentView.getSelectedText();
```

</div>

</div>


> **NOTE**  
> If there is no selected text in the document then a `null` value will be returned.
>


[Activity event]: #activity-events
[Listeners]: #document-listeners
[Android Activity Lifecycle]: https://developer.android.com/guide/components/activities/activity-lifecycle
[Custom UI]: android-pdf-viewing.html#custom-ui
