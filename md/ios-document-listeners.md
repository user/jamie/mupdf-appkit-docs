# Document Listeners

<div class="banner">
    <div class="ios-text"></div>
    <div class="vendor-logo ios single"></div>
</div>

##  

When developing your own custom UI, your application's `ViewController` should implement the `ARDKBasicDocViewDelegate` and `ARDKDocumentEventTarget` protocols to intercept events.


Additionally, you can listen for basic `success` or `error` for a document load.


## ARDKBasicDocViewDelegate


### Document completed

Called when the document has completely loaded.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
/// Inform the UI that both the loading of the document
/// and the initial rendering have completed. An app might
/// display a busy indicator while a document is initially
/// loading, and use this delegate method to dismiss the
/// indicator.
func loadingAndFirstRenderComplete() {

}
```

</div>

<div class="objc">




```
/// Inform the UI that both the loading of the document
/// and the initial rendering have completed. An app might
/// display a busy indicator while a document is initially
/// loading, and use this delegate method to dismiss the
/// indicator.
- (void)loadingAndFirstRenderComplete {

}
```

</div>

</div>



### UI update

Called when the document changes selection state and the UI should update appropriately.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
/// Tell the UI to update according to changes in the
/// selection state of the document. This allows an app
/// to refresh any currently displayed state information.
/// E.g., a button used to toggle whether the currently
/// selected text is bold, may show a highlight to indicate
/// bold or not. This call would be the appropriate place to
/// ensure that highlight reflects the current state.
func updateUI {

}
```

</div>

<div class="objc">




```
/// Tell the UI to update according to changes in the
/// selection state of the document. This allows an app
/// to refresh any currently displayed state information.
/// E.g., a button used to toggle whether the currently
/// selected text is bold, may show a highlight to indicate
/// bold or not. This call would be the appropriate place to
/// ensure that highlight reflects the current state.
- (void)updateUI {

}
```

</div>

</div>


### Page change

Called on a page change event - i.e. when the document has scrolled or jumped to another page.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
/// Tell the UI that the document has scrolled to a new page.
/// An app may use this to update a label showing the current
/// displayed page number or to scroll a view of thumbnails
/// to the correct page.
func viewDidScroll(toPage page: Int) {

}
```

</div>

<div class="objc">




```
/// Tell the UI that the document has scrolled to a new page.
/// An app may use this to update a label showing the current
/// displayed page number or to scroll a view of thumbnails
/// to the correct page.
- (void)viewDidScrollToPage:(NSInteger)page {

}
```

</div>

</div>



### Scrolling completed

Called when a user scrolling event has concluded it's animation.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
/// Tell the delegate when a scrolling animation concludes.
/// This can be used like viewDidScrollToPage, but for more
/// intensive tasks that one wouldn't want to run repeatedly
/// during scrolling.
func scrollViewDidEndScrollingAnimation {

}
```

</div>

<div class="objc">




```
/// Tell the delegate when a scrolling animation concludes.
/// This can be used like viewDidScrollToPage, but for more
/// intensive tasks that one wouldn't want to run repeatedly
/// during scrolling.
- (void)scrollViewDidEndScrollingAnimation {

}
```

</div>

</div>




### Swallowing tap selections

An application developer has the ability to intercept tap events on the document to prevent a default selection from occurring. To do this the following delegate methods need to return `false`. Essentially a document is in a _read only_ state in this mode.


<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
/// Offer the UI the opportunity to swallow a tap that
/// may have otherwise caused selection. Return YES
/// to swallow the event. This is not called for taps
/// over links or form fields. An app might use this to
/// provide a way out of a special mode (full-screen for
/// example). In that case, if the app is using the tap to
/// provoke exit from full-screen mode, then it would return
/// YES from this method to avoid the tap being interpreted
/// also by the main document view.
func swallowSelectionTap() -> Bool {
    return false
}

/// Offer the UI the opportunity to swallow a double tap that
/// may have otherwise caused selection. Return YES to swallow
/// the event. This is not called for double taps over links
/// or form fields. An app might use this in a way similar to
/// that appropriate to swallowSelectionTap.
func swallowSelectionDoubleTap() -> Bool {
    return false
}
```

</div>

<div class="objc">




```
/// Offer the UI the opportunity to swallow a tap that
/// may have otherwise caused selection. Return YES
/// to swallow the event. This is not called for taps
/// over links or form fields. An app might use this to
/// provide a way out of a special mode (full-screen for
/// example). In that case, if the app is using the tap to
/// provoke exit from full-screen mode, then it would return
/// YES from this method to avoid the tap being interpreted
/// also by the main document view.
- (BOOL)swallowSelectionTap {
    return NO;
}

/// Offer the UI the opportunity to swallow a double tap that
/// may have otherwise caused selection. Return YES to swallow
/// the event. This is not called for double taps over links
/// or form fields. An app might use this in a way similar to
/// that appropriate to swallowSelectionTap.
- (BOOL)swallowSelectionDoubleTap {
    return NO;
}
```

</div>

</div>




> **NOTE**  
> It is important that these delegate methods return `true` by default for expected text and annotation selection behaviour to occur.
>


### Inhibiting the keyboard

If required an application developer can prevent the keyboard from appearing.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
/// Called to allow the delegate to inhibit the keyboard. An app
/// might use this in special modes where there is limited
/// vertical space, so as to avoid the keyboard appearing.
func inhibitKeyBoard -> Bool {
    return false
}
```

</div>

<div class="objc">




```
/// Called to allow the delegate to inhibit the keyboard. An app
/// might use this in special modes where there is limited
/// vertical space, so as to avoid the keyboard appearing.
- (BOOL)inhibitKeyBoard {
    return NO;
}
```

</div>

</div>



### Opening a URL

This method is called when the document interaction invokes a URL to open.


<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
/// The document view calls this when
/// a link to an external document is tapped.
func callOpenUrlHandler(_ url: URL!, fromVC presentingView: UIViewController!) {

}
```

</div>

<div class="objc">




```
- (void)callOpenUrlHandler:(NSURL *)url fromVC:(UIViewController *)presentingView {

}
```

</div>

</div>


## ARDKDocumentEventTarget

### Page load events and loading complete

Called as pages are loaded from the document.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
/// Called as pages are loaded from the document.
/// There may be further calls, e.g., if pages
/// are added or deleted from the document.
func updatePageCount(_ pageCount: Int, andLoadingComplete complete: Bool) {

}
```

</div>

<div class="objc">




```
/// Called as pages are loaded from the document.
/// There may be further calls, e.g., if pages
/// are added or deleted from the document.
- (void)updatePageCount:(NSInteger)pageCount andLoadingComplete:(BOOL)complete {

}
```

</TabItem>

</Tabs>


### Selection changes

Called when a selection is made within the document, moved or removed.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
func selectionHasChanged {

}

// there is also a function method which can be
// associated against the instance of `MuPDFDKDoc` as follows
doc.onSelectionChanged = {

}
```

</div>

<div class="objc">




```
- (void)selectionHasChanged {

}

// there is also a function method which can be
// associated against the instance of `MuPDFDKDoc` as follows
doc.onSelectionChanged = ^() {

};
```

</div>

</div>


### Selection types


Once a selection has been made on a document it might be necessary to understand what type of selection it is and if there is any further data. For example, is the user's selection a redaction annotation or is it a note annotation? Does the selected annotation have a date associated with it?

To determine this information, the following type of query against the document instance (`MuPDFDKDoc`) can be made:


<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
let selectionIsRedaction:Bool = doc.selectionIsRedaction
let selectionIsNote:Bool = doc.selectionIsAnnotationWithText
let selectedAnnotationsDate:Date? = doc.selectedAnnotationsDate
```

</div>

<div class="objc">




```
BOOL selectionIsRedaction = doc.selectionIsRedaction;
BOOL selectionIsNote = doc.selectionIsAnnotationWithText;
NSDate* selectedAnnotationsDate = doc.selectedAnnotationsDate;
```

</div>

</div>


The following table defines the full set of available selections:

| variable name | type | description | return type |  
|---|---|---|---|
| selectionIsWidget | form widget | _Whether a form widget is currently selected_ | `Bool` |
| selectedAnnotationsText | text | _The text string associated with the selected annotation_ | `String` or `nil` |
| selectedAnnotationsDate | date | _The date associated with the selected annotation_ | `Date` or `nil` |
| selectedAnnotationsAuthor | author | _The author of the selected annotation_ | `String` or `nil` |
| selectionIsRedaction | redaction | _Whether a redaction annotation is selected_ | `Bool` |  
| selectionIsTextHighlight | highlight | _Whether a text highlight annotation is selected_ | `Bool` |
| selectionIsAnnotationWithText | note | _Whether an annotation that has text is selected_ | `Bool` |
| haveTextSelection | text | _Whether text is currently selected_ | `Bool` |  
| haveAnnotationSelection | any | _Whether an annotation is currently selected_ | `Bool` |


## Document load listeners

When a document load is requested, the following function blocks can be defined for the document (i.e. the instance of `MuPDFDKDoc`).

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
doc.successBlock = {

}

doc.errorBlock = {(error:ARDKDocErrorType?) in

}
```

</div>

<div class="objc">




```
doc.successBlock = ^() {

};

doc.errorBlock = ^(ARDKDocErrorType error) {

};
```

</div>

</div>



[Default UI]: ios-pdf-viewing.html#default-ui
[Custom UI]: ios-pdf-viewing.html#custom-ui
[UIViewController]: https://developer.apple.com/documentation/uikit/uiviewcontroller
