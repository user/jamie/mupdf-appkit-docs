# PDF Annotations

<div class="banner">
    <div class="android-text"></div>
    <div class="vendor-logo android single"></div>
</div>


## Annotations

Annotations include allowing [drawing], [adding notes] and [highlighting] text on a PDF document.

## Draw mode

Turning on drawing is as simple as calling the `setDrawModeOn()` method against your `DocumentView` instance. To turn off drawing just call the `setDrawModeOff()` method against your `DocumentView` instance.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun setDocumentDrawMode(documentView:DocumentView, enable:Boolean) {
	if (enable) {
		documentView.setDrawModeOn()
	} else {
		documentView.setDrawModeOff()
	}
}
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void setDocumentDrawMode(documentView:DocumentView, enable:Boolean) {
	if (enable) {
		documentView.setDrawModeOn();
	} else {
		documentView.setDrawModeOff();
	}
}
```

</div>

</div>


When Draw Mode is enabled, the user can draw an ink annotation with the selected line thickness and color. When Draw mode is then disabled, the annotation is saved to the document.

### Line thickness

To set the line thickness send through the thickness value as a `float` using the `setLineThickness()` API as follows:

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun setDocumentDrawLineThickness(documentView:DocumentView, thickness:float) {
	documentView.setLineThickness(thickness)
}
// setting line thickness to 10 points
setDocumentDrawLineThickness(myNUIDocumentView, 10)
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void setDocumentDrawLineThickness(documentView:DocumentView, thickness:float) {
	documentView.setLineThickness(thickness);
}
// setting line thickness to 10 points
setDocumentDrawLineThickness(myNUIDocumentView, 10);
```

</div>

</div>


### Line color

To set the line color send through the hex value for the color in an AARRGGBB `integer` format using the `setLineColor()` API as follows:

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun setDocumentDrawLineColor(documentView:DocumentView, color:int) {
	documentView.setLineColor(color)
}
// setting line color to green
setDocumentDrawLineColor(myNUIDocumentView, 0xFF00FF00)
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void setDocumentDrawLineColor(documentView:DocumentView, color:int) {
	documentView.setLineColor(color);
}
// setting line color to green
setDocumentDrawLineColor(myNUIDocumentView, 0xFF00FF00);
```

</div>

</div>


## Note mode

Depending on the mode the user will be able to add notes onto the document or not.

To turn on note mode call the `setNoteModeOn()` method against your `DocumentView` instance. To turn off note mode just call the corresponding `setNoteModeOff()` method against your `DocumentView` instance.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun setDocumentNoteMode(documentView:DocumentView, enable:Boolean) {
	if (enable) {
		documentView.setNoteModeOn()
	} else {
		documentView.setNoteModeOff()
	}
}
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void setDocumentNoteMode(documentView:DocumentView, enable:Boolean) {
	if (enable) {
		documentView.setNoteModeOn();
	} else {
		documentView.setNoteModeOff();
	}
}
```

</div>

</div>


## Highlighting

In order to create a highlight annotation on some text in a PDF document, you must first have some text selected. Once selected call the `highlightSelection()` method against your `DocumentView` instance.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun setDocumentHighlight(documentView:DocumentView)  {
	documentView.highlightSelection()
}
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void setDocumentHighlight(documentView:DocumentView) {
	documentView.highlightSelection();
}
```

</div>

</div>

> **NOTE**  
> Without a text selection being present in your document view calling `highlightSelection()` will have no effect.
>


## Deleting annotations

Essentially all annotations are treated the same as simply _selections_ once they have been selected by a user. This _selection_ can then be removed by calling the `deleteSelection()` method against the `DocumentView` instance.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun deleteDocumentSelection(documentView:DocumentView)  {
	documentView.deleteSelection()
}
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void deleteDocumentSelection(documentView:DocumentView) {
	documentView.deleteSelection();
}
```

</div>

</div>

> **NOTE**  
> Without an annotation being currently selected in your document view calling `deleteSelection()` will have no effect.
>

## Author

To view and/or edit the author name for an annotation an application developer should call the `author()` method against the `DocumentView` instance. An internal `DocumentView` UI will then be invoked to view and edit the author name.

![author-ui](images/android/author-ui.png)
_The default UI for setting Author name_


<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun openAuthor(documentView:DocumentView) {
    documentView.author()
}
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void openAuthor(documentView:DocumentView) {
    documentView.author();
}
```

</div>

</div>




[drawing]: #draw-mode
[adding notes]: #note-mode
[highlighting]: #highlighting
