# Getting Started

<div class="banner">
    <div class="android-text"></div>
    <div class="vendor-logo android single"></div>
</div>

## System requirements

- **Android minimum SDK**: The MuPDF library needs Android version 5.0 or newer. Make sure that the `minSdkVersion` in your app's `build.gradle` is at least 21.

```
android {
	defaultConfig {
		minSdkVersion 21
		...
	}
	...
}
```

## Adding the App Kit to your project

In order to include MuPDF in your app you need to use the Gradle build system.

The MuPDF App Kit can be retrieved as pre-built artifacts from a local `app/libs` folder location relative to your Android project. Your Android project's module `build.gradle` should add this location.

To add the MuPDF App Kit to your project add the dependencies section in your `Module` `build.gradle` (not the `Project` `build.gradle`)

```
dependencies {
	// ensure these artifacts exist in your app/libs folder
	implementation(name:'sodk_resources', ext:'aar')
    implementation(name:'editor', ext:'aar')
    implementation(name:'solib', ext:'aar')
    implementation(name:'wheel', ext:'aar')
    implementation(name:'mupdf', ext:'aar')
}

repositories {flatDir {dirs 'libs'}}
```
<span class="smallFilename">build.gradle (Module)</span><p/>

### Fetching the artifacts

Ensure to follow these steps:

- [Download the latest MuPDF App Kit]

- Then copy the 5 `aar` files contained in `mupdf-test/app/libs` to your own app's `app/libs/` folder.

- Sync your `build.gradle` and then then the App Kit libraries should be correctly configured and ready for use.

## Verify your integration

To ensure you have correctly added the library to your project ensure that your source code files can reference `import com.artifex.sonui.editor.NUIView` without error.

## License key
To remove the MuPDF document watermark from your App, you will need to use a license key to activate App Kit.

To acquire a license key for your app you should:

1. Go to [artifex.com/appkit]
2. Purchase your license(s)
3. In your application code add the API call to activate the license

### Using your license key

If you have a license key for MuPDF App Kit, it will be bound to the App ID which you will have defined at the time of purchase. Therefore you should ensure that the App ID in your Android project is correctly set. This is defined in configuration within the __Gradle__ file for the app (`build.gradle(:app)`).

```
defaultConfig {
        applicationId "your.license.key.app.id"
        ...
    }
```
<span class="smallFilename">build.gradle(:app)</span><p/>

Once you have confirmed that the `applicationId` is correctly named, then call the following API early on in your application code:


<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView
...

val ctx: Context = this
val licenseKey:String = "put your license key here"
val ok:Boolean = DocumentView.unlockAppKit(ctx, licenseKey)
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;
...

Context ctx = this;
String licenseKey = "put your license key here";
boolean ok = DocumentView.unlockAppKit(ctx, licenseKey);
```

</div>

</div>



> **NOTE**  
> The `Context` parameter should reference your main application `Activity`.
>
> The API call to set the license key should be made before you instantiate an App Kit `DocumentView`
>

[artifex.com/appkit]: https://artifex.com/appkit
[Download the latest MuPDF App Kit]: download.html
