# Bitmap Exporting

<div class="banner">
    <div class="android-text"></div>
    <div class="vendor-logo android single"></div>
</div>


##

An application developer can export a document's pages into bitmap format by invoking the `MuPDF` library directly, opening the document and processing the resulting pages into a bitmap file sequence.

## Loading MuPDF without a UI

To load the `MuPDF` library and use it directly an application developer should import `SODKLib` and request a document load as follows:

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.solib.SODKLib

// use the registered configuration options of the application as
// the document configuration options.
var docCfg:ConfigOptions = SODKLib.getAppConfigOptions()
var lib:SODKLib = SODKLib.getLibraryForPath(activity, document_file_path)

// Load the document.
mDoc = lib.openDocument(document_file_path, object : DocumentListener {
    override fun onPageLoad(int pageNum){}
    override fun onDocCompleted() {}
    override fun onError(error:Int, errorNum:Int){}
    override fun onSelectionChanged(startPage:Int, endPage:Int){}
    override fun onLayoutCompleted(){}

},activity, docCfg)

private abstract class GeneratePngsTask() : AsyncTask<Void, Void, Boolean>() {
    // See Sample app
}
```

</div>

<div class="java">



```
import com.artifex.solib.SODKLib;

// use the registered configuration options of the application as
// the document configuration options.
ConfigOptions docCfg = SODKLib.getAppConfigOptions();

SODKLib lib = SODKLib.getLibraryForPath(activity, document_file_path);

// Load the document.
mDoc = lib.openDocument(document_file_path, new SODocLoadListener()
{
    @Override
    public void onPageLoad(int pageNum)
    {
        /*
         * This could be useful if you want to display
         * progress as the document is loading.
         */
    }

    @Override
    public void onDocComplete()
    {
        // onDocComplete is called twice, we only need the first one.
        if (!mCompleted && !mCancelled)
        {
            /*
             * In an AsyncTask extract page bitmaps sequentially and
             * compress them to PNG format.
             */
            GeneratePngsTask pngTask = new GeneratePngsTask();
            pngTask.execute((Void)null);
        }

        mCompleted = true;
    }

    @Override
    public void onError(final int error, final int errorNum)
    {
        // Called when the document load fails.
    }

    @Override
    public void onSelectionChanged(final int startPage,
                                   final int endPage)
    {
        // Called when the selection changes
    }

    @Override
    public void onLayoutCompleted()
    {
        // Called when a core layout is done
    }
}, activity, docCfg);

private class GeneratePngsTask extends AsyncTask<Void, Void, Boolean>
{
    // See Sample app
}
```

</div>

</div>

By using the document load listeners an application developer should be able use a background task to process document pages as required and use the `createBitmapForPath` method of the `SODKLib` to export pages in bitmap format.

[Android Sample app]: download.html#android
