# Document Options & Listeners

<div class="banner">
    <div class="android-text"></div>
    <div class="vendor-logo android single"></div>
</div>


## Options

An application developer can register options for the following in the MuPDF SDK:

|Property|Capability|
|--|--|
|Editing|_Enable_|
|Save As|_Enable_|
|Open In|_Enable_|
|Sharing|_Enable_|
|External Clipboard In|_Enable_|
|External Clipboard Out|_Enable_|
|Printing|_Enable_|
|Launch External Url|_Enable_|
|Form Filling|_Enable_|
|Form Signing|_Enable_|
|Redactions|_Enable_|
|Full Screen|_Enable_|
|Invert Content In Dark Mode|_Enable_|



To do so a developer should instantiate `ConfigOptions`, set the required variables within that object, and then register it against the `SODKLib` object.

The following code example disables editing on a document:

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.solib.ConfigOptions
import com.artifex.solib.SODKLib

fun setupConfigOptions() {
    var configOptions:ConfigOptions = ConfigOptions()
    configOptions.isEditingEnabled = false
    SODKLib.setAppConfigOptions(configOptions)
}
```

</div>

<div class="java">



```
import com.artifex.solib.ConfigOptions;
import com.artifex.solib.SODKLib;

public void setupConfigOptions() {
    ConfigOptions configOptions = new ConfigOptions();
    configOptions.setEditingEnabled(false);
    SODKLib.setAppConfigOptions(configOptions);
}
```

</div>

</div>


## Listeners


Document listeners should only be required when using the [Custom UI] as the application developer is responsible for providing their own UI to manage relevant document events.

Available document listeners are as follows:

### Page loaded

Called when pages are loaded.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
mDocumentView.setDocumentListener(object : DocumentView.DocumentListener {
    override fun onPageLoaded(pagesLoaded: Int) {

    }
}
```

</div>

<div class="java">



```
mDocumentView.setDocumentListener(new DocumentView.DocumentListener() {
    @Override
    public void onPageLoaded(int pagesLoaded) {

    }
}
```

</div>

</div>


### Document completed

Called when the document has completely loaded.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
mDocumentView.setDocumentListener(object : DocumentView.DocumentListener {
    override fun onDocCompleted() {

    }
}
```

</div>

<div class="java">



```
mDocumentView.setDocumentListener(new DocumentView.DocumentListener() {
    @Override
    public void onDocCompleted() {

    }
}
```

</div>

</div>


### Password required

Called when a password is required by the document.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
mDocumentView.setDocumentListener(object : DocumentView.DocumentListener {
    override fun onPasswordRequired() {

    }
}
```

</div>

<div class="java">



```
mDocumentView.setDocumentListener(new DocumentView.DocumentListener() {
    @Override
    public void onPasswordRequired() {

    }
}
```

</div>

</div>


> **NOTE**  
>To submit a password an application should call `mDocumentView.providePassword(<String>)`.
>



### Page change

Called on a page change event - i.e. when the document has scrolled or jumped to another page.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
mDocumentView.setPageChangeListener { pageNumber ->

}
```

</div>

<div class="java">



```
mDocumentView.setPageChangeListener(new DocumentView.ChangePageListener() {
    @Override
    public void onPage(int pageNumber) {

    }
})
```

</div>

</div>



### Full screen mode

A `DocumentView` document has the ability to fill the screen and enter a _uneditable_ mode for an optimal reading experience. It is the application developer's responsibility to turn off the UI that they do not wish to see when this mode is invoked and to ensure that their `DocumentView` instance fills the device screen. To turn desired UI back on again the `DocumentView` instance will invoke the application developer's closure method upon exiting full screen mode ( when the user taps the screen ).

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
findViewById<View>(R.id.button_full_screen).setOnClickListener {
    // hide this activity's UI
    findViewById<View>(R.id.custom_ui_layout).visibility = View.GONE

    // put DocumentView in full screen mode
    if (mDocumentView != null) {
        mDocumentView.enterFullScreen {
            // closure method to restore our UI upon exit
            findViewById<View>(R.id.ui_layout).visibility = View.VISIBLE
        }
    }
}
```

</div>

<div class="java">



```
findViewById(R.id.button_full_screen).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        // hide this activity's UI
        findViewById(R.id.custom_ui_layout).setVisibility(View.GONE);

        // put DocumentView in full screen mode
        if (mDocumentView != null) {
            mDocumentView.enterFullScreen(new Runnable() {
                @Override
                public void run() {
                    // closure method to restore our UI upon exit
                    findViewById(R.id.ui_layout).setVisibility(View.VISIBLE);
                }
            });
        }
    }
});
```

</div>

</div>


### Done

Called when the document is closed.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
mDocumentView.setDocumentListener(object : DocumentView.DocumentListener {
    override fun onDone() {

    }
}
```

</div>

<div class="java">



```
mDocumentView.setDocumentListener(new DocumentView.DocumentListener() {
    @Override
    public void onDone() {

    }
}
```

</div>

</div>

[Activity event]: #activity-events
[Listeners]: #document-listeners
[Android Activity Lifecycle]: https://developer.android.com/guide/components/activities/activity-lifecycle
[Custom UI]: android-pdf-viewing.html#custom-ui
