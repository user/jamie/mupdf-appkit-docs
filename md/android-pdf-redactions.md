# PDF Redactions

<div class="banner">
    <div class="android-text"></div>
    <div class="vendor-logo android single"></div>
</div>

## Redactions

The redaction feature has been designed to work on text, images, and links. By selecting all or part of an image, text, or link, and applying redaction this then permanently redacts the selected information, making it impossible to retrieve the original data.

Redacted text and areas can be [marked], and any marked redactions can be [removed], however once the redactions are [applied] and the document is saved then the redacted information is blocked out with black to denote the area of redaction. At this point the original text and/or area is unretrievable.

### Example

|![marked for redaction](images/android/marked-for-redaction.png)|![redaction applied](images/android/redaction-applied.png)|
|--|--|
|Text marked for redaction|Redaction applied|


## Marking text for redaction

To mark text for redaction call the `redactMarkText()` method against the `DocumentView` instance. The document's currently selected text will then be marked with a red-outlined box to denote the redaction demarcation.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun markForTextRedaction(documentView:DocumentView) {
    documentView.redactMarkText()
}
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void markForTextRedaction(documentView:DocumentView) {
    documentView.redactMarkText();
}
```

</div>

</div>




## Marking an area for redaction

To mark an area for redaction call the `redactMarkArea()` method against the `DocumentView` instance. The document will then enter a mode whereby the user can then draw a red-outlined boxed area to denote the redaction demarcation.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun markForAreaRedaction(documentView:DocumentView) {
    documentView.redactMarkArea()
}
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void markForAreaRedaction(documentView:DocumentView) {
    documentView.redactMarkArea();
}
```

</div>

</div>




## Removing a marked redaction

To remove a marked redaction call the `redactRemove()` method against the `DocumentView` instance. The document's currently selected _marked for redaction_ ( i.e. a selection of text or an area with the red box outline ) will then no longer be marked for redaction.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun removeMarkForRedaction(documentView:DocumentView) {
    documentView.redactRemove()
}
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void removeMarkForRedaction(documentView:DocumentView) {
    documentView.redactRemove();
}
```

</div>

</div>



## Applying redactions

Applying redactions means that all document selections which are _marked for redaction_ will be redacted. To apply redactions call the `redactApply()` method against the `DocumentView` instance. Once redactions have been applied then the redacted text is blocked out with black and the red redaction demarcation areas are removed.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
import com.artifex.sonui.editor.DocumentView

fun applyRedactions(documentView:DocumentView) {
    documentView.redactApply()
}
```

</div>

<div class="java">



```
import com.artifex.sonui.editor.DocumentView;

public void applyRedactions(documentView:DocumentView) {
    documentView.redactApply();
}
```

</div>

</div>


> **NOTE**  
> Redactions are not permanently set until the document is saved. If the document is exited without saving then the redactions will be lost.
>

[marked]: #marking-text-for-redaction
[removed]: #removing-a-marked-redaction
[applied]: #applying-redactions
