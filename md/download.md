# Download

<div class="banner intro">
	<div class="default-text"></div>
	<div class="vendor-logo mupdf"></div>
	<div class="vendor-logo android"></div>
	<div class="vendor-logo ios"></div>
</div>


Artifex supplies projects, available to try for free, which utilize App Kit for both Android and iOS. Please use Android Studio or Apple's Xcode to try the projects out.

The sample projects contain the App Kit libraries, which are built using the corresponding version of MuPDF.


The demo version of MuPDF App Kit contains a watermark. To remove the watermark, you will need to purchase a commercial license from Artifex. Instructions on using the key to remove the watermark can be found in the __Getting Started__ sections of this documentation.

## Android

[Android App Kit]

## iOS

[iOS App Kit]


[Android App Kit]: https://www.mupdf.com/downloads/appkit/archive/AppKit-v2.0.0-Android.zip
[iOS App Kit]: https://www.mupdf.com/downloads/appkit/archive/AppKit-v2.0.0-iOS.zip
