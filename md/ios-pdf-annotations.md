# PDF Annotations

<div class="banner">
    <div class="ios-text"></div>
    <div class="vendor-logo ios single"></div>
</div>

## Annotations

When developing your own custom UI, you may wish to support the creation of annotations, including [drawing], [adding notes] and [highlighting] text on a PDF document.

## Draw mode

Turning on drawing is as simple as setting the annotating mode to `MuPDFDKAnnotatingMode_Draw` against your `MuPDFDKBasicDocumentViewController` instance. To turn off drawing mode just set the annotating mode to `MuPDFDKAnnotatingMode_None` against your `MuPDFDKBasicDocumentViewController` instance.

The following code toggles draw mode on and off and might serve as the handler for one of the buttons in your UI.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
if basicDocVc.annotatingMode == MuPDFDKAnnotatingMode_Draw {
	basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_None
} else {
	basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_Draw
}
```

</div>

<div class="objc">



```
if (basicDocVc.annotatingMode == MuPDFDKAnnotatingMode_Draw) {
	basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_None;
}
else {
	basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_Draw;
}
```

</div>

</div>



When [Draw mode] is enabled, the user can draw an ink annotation with the selected line thickness and color. When [Draw mode] is then disabled, the annotation is saved to the document.



### Line thickness

To set the line thickness set the `inkAnnotationThickness` value as a `CGFloat`.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
basicDocVc.inkAnnotationThickness = 10
```

</div>

<div class="objc">



```
basicDocVc.inkAnnotationThickness = 10;
```

</div>

</div>



### Line color

To set the line color set the `inkAnnotationColor` value as a `UIColor`.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
basicDocVc.inkAnnotationColor = .green
```

</div>

<div class="objc">



```
basicDocVc.inkAnnotationColor = [UIColor green];
```

</div>

</div>



## Note mode

To turn on note mode set the annotating mode to `MuPDFDKAnnotatingMode_Note` against your `MuPDFDKBasicDocumentViewController` instance. To save the note set the annotating mode to `MuPDFDKAnnotatingMode_None`.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_Note
```

</div>

<div class="objc">



```
basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_Note;
```

</div>

</div>


## Highlighting

To turn on highlighting set the annotating mode to `MuPDFDKAnnotatingMode_HighlightTextSelect` against your `MuPDFDKBasicDocumentViewController` instance. When in text-highlighting mode, the document view will allow the user to create a text highlight by dragging across text, whereupon the mode will revert back to `MuPDFDKAnnotatingMode_None`, leaving a newly created annotation selected. The selected annotation will show drag handles which the user can use to adjust it.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_HighlightTextSelect
```

</div>

<div class="objc">



```
basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_HighlightTextSelect;
```

</div>

</div>

> **NOTE**  
> Highlight mode will only affect the actively selected text before the mode is activated.
>


## Deleting annotations

Essentially all annotations are treated the same as simply _selections_ once they have been selected by a user. This _selection_ can then be removed by calling the `deleteSelectedAnnotation()` method against the `MuPDFDKDoc` instance within `MuPDFDKBasicDocumentViewController`.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
func deleteDocumentSelection() {
	let myDoc:MuPDFDKDoc? = basicDocVc?.session.doc as! MuPDFDKDoc
	myDoc?.deleteSelectedAnnotation()
}
```

</div>

<div class="objc">



```
-(void)deleteDocumentSelection {
	MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
	[myDoc deleteSelectedAnnotation];
}
```

</div>

</div>



Without an annotation being currently selected, calling `deleteSelectedAnnotation()` will have no effect.

## Author

To set the author name for subsequent annotation creations, an application developer should set the `documentAuthor` property to the required string value within the `MuPDFDKDoc` instance.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">




```
func setAuthor(name:String) {
	let myDoc:MuPDFDKDoc? = basicDocVc?.session.doc as! MuPDFDKDoc
	myDoc?.documentAuthor = name
}
```

</div>

<div class="objc">



```
-(void)setAuthor:(NSString *)name {
	MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
	myDoc.documentAuthor = name;
}
```

</div>

</div>




[drawing]: #draw-mode
[Draw mode]: #draw-mode
[adding notes]: #note-mode
[highlighting]: #highlighting
