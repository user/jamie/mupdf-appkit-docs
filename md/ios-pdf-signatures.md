# PDF Signatures

<div class="banner">
    <div class="ios-text"></div>
    <div class="vendor-logo ios single"></div>
</div>


## Overview

An implementer needs access to a system that can perform the PKCS7 based signing and verification. This system must be used to implement the protocol `ARDKDigitalSigningDelegate` and supply an instance by setting the `ARDKDocSession signingDelegate` property. Please refer to the iOS SDK and the implementation of the `ARDKDigitalSigningDelegate` based on OpenSSL.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
self.session.signingDelegate = SigningDelegate()

/**
 * Implement signing as required
 */
class SigningDelegate:NSObject, ARDKDigitalSigningDelegate {
    func createSigner(_ presentingViewController: UIViewController!,
                        onComplete: ((ARDKSigner?) -> Void)!) {

    }

    func createVerifier(_ presentingViewController: UIViewController!,
                        onComplete: ((ARDKVerifier?) -> Void)!) {

    }

    func presentVerifyResult(_ presentingViewController: UIViewController!,
        verifyResult: PKCS7VerifyResult,
        invalidChangePoint: Int32,
        designatedName: PKCS7DesignatedName!,
        description: PKCS7Description!,
        onComplete: (() -> Void)!) {

    }
}
```

</div>

<div class="objc">



```
self.session.signingDelegate = [[SigningDelegate alloc] init];
@implementation SigningDelegate

/**
 * Implement signing as required
 */
- (void)createSigner:(UIViewController *)presentingViewController
    onComplete:(void (^)(id<PKCS7Signer>))onComplete {

}

- (void)createVerifier:(UIViewController *)presentingViewController
    onComplete:(void (^)(id<PKCS7Verifier>))onComplete {

}

- (void)presentVerifyResult:(UIViewController *)presentingViewController
    verifyResult:(PKCS7VerifyResult)verifyResult
    invalidChangePoint:(int)invalidChangePoint
    designatedName:(id<PKCS7DesignatedName>)designatedName
    description:(id<PKCS7Description>)description
    onComplete:(void (^)(void))onComplete {

}

@end
```

</div>

</div>
