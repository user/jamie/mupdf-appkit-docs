# PDF Table of Contents

<div class="banner">
    <div class="android-text"></div>
    <div class="vendor-logo android single"></div>
</div>



## Overview

Not all PDF documents will have a Table of Contents, to check to see if the current document instance contains a Table of Contents an application developer should call the `isTOCEnabled` method once the document has fully loaded.

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
val hasTOC:Boolean = documentView.isTOCEnabled

if (hasTOC) {
    // enable UI (e.g. button/gesture) responsible for invoking TOC display
}
```

</div>

<div class="java">


```
boolean hasTOC = documentView.isTOCEnabled();

if (hasTOC) {
    // enable UI (e.g. button/gesture) responsible for invoking TOC display
}
```

</div>

</div>


In order to show the Table of Contents UI an application developer simply needs to call the `tableOfContents` method as follows:

<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
documentView.tableOfContents()
```

</div>

<div class="java">



```
documentView.tableOfContents();
```

</div>

</div>

![table-of-contents-ui](images/android/toc-ui.png)
_Table of Contents UI_


> **NOTE**  
>Table of Contents is also known as "Bookmarks"
>
