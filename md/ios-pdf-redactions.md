# PDF Redactions


<div class="banner">
    <div class="ios-text"></div>
    <div class="vendor-logo ios single"></div>
</div>


## Redactions

The redaction feature has been designed to work on text, images, and links. By selecting all or part of an image, text, or link permanently redacts the selected information, making it impossible to retrieve the original data.

Redacted text and areas can be [marked], and any marked redactions can be [removed], however once the redactions are [applied] and the document is saved then the redacted information is blocked out with black to denote the area of redaction. At this point the original text and/or area is unretrievable.

### Example

|![marked for redaction](images/android/marked-for-redaction.png)|![redaction applied](images/android/redaction-applied.png)|
|--|--|
|Text marked for redaction|Redaction applied|


## Redaction modes

Your custom UI can support redaction by employing three specific annotating modes.

### Edit redaction mode

Firstly set the annotation mode for your document view to `MuPDFDKAnnotatingMode_EditRedaction`. This ensures that only redaction markings can be selected. When selected they can be adjusted or removed.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_EditRedaction
```

</div>

<div class="objc">



```
basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_EditRedaction;
```

</div>

</div>


After adding a redaction it is advised to return your `MuPDFDKBasicDocumentViewController` back to this edit mode as default.

### Text redaction mode

Set the annotation mode for your document to `MuPDFDKAnnotatingMode_RedactionTextSelect`. This prepares the document view to mark text for redaction.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_RedactionTextSelect
```

</div>

<div class="objc">



```
basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_RedactionTextSelect;
```

</div>

</div>


In this mode, the user can create a redaction marking by dragging across text, whereupon the mode will revert to `MuPDFDKAnnotatingMode_EditRedaction`, leaving the newly created marking of the text selected. The selection will show drag handles via which the user can adjust it.

### Area redaction mode

Set the annotation mode for your document to `MuPDFDKAnnotatingMode_RedactionAreaSelect`. This prepares the document view to mark an area for redaction.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_RedactionAreaSelect
```

</div>

<div class="objc">



```
basicDocVc.annotatingMode = MuPDFDKAnnotatingMode_RedactionAreaSelect;
```

</div>

</div>



In this mode, the user can create a redaction marking by dragging out an area from one corner to the diagonally opposite one, whereupon the mode will revert to `MuPDFDKAnnotatingMode_EditRedaction`, leaving the newly created marking of an area selected. The selection will show drag handles via which the user can adjust it.

## Marking already selected text

There is an alternative way to mark text for redaction. If the user has already selected an area of text, it is possible to mark that text for redaction by calling the `addRedactAnnotation()` method against your `MuPDFDKDoc` instance. The document's currently selected text will then be marked with a red box outline to denote the redaction demarcation.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
let myDoc:MuPDFDKDoc = basicDocVc.session.doc as! MuPDFDKDoc
myDoc.addRedactAnnotation()    
```

</div>

<div class="objc">



```
MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
[myDoc addRedactAnnotation];
```

</div>

</div>




## Removing a marked redaction

To remove a marked redaction call the `deleteSelectedAnnotation()` method against your `MuPDFDKDoc` instance. The document's currently selected _marked for redaction_ ( i.e. a selection of text with the red box outline ) will then no longer be marked for redaction.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
let myDoc:MuPDFDKDoc = basicDocVc.session.doc as! MuPDFDKDoc
myDoc.deleteSelectedAnnotation()
```

</div>

<div class="objc">



```
MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
[myDoc deleteSelectedAnnotation];
```

</div>

</div>



## Applying redactions

Applying redactions means that all document selections which are _marked for redaction_ will be redacted. To apply redactions call the `finalizeRedactAnnotations()` method against your `MuPDFDKDoc` instance. Once redactions have been applied then the redacted text is blocked out with black.

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
let myDoc:MuPDFDKDoc = basicDocVc.session.doc as! MuPDFDKDoc
myDoc.finalizeRedactAnnotations({
    // onComplete
})
```

</div>

<div class="objc">


```
MuPDFDKDoc *myDoc = (MuPDFDKDoc *)basicDocVc.session.doc;
[myDoc finalizeRedactAnnotations:^{
    // onComplete
}];
```

</div>

</div>




> **NOTE**  
> Redactions are not permanently set until the document is saved. If the document is exited without saving then the redactions will be lost.
>

[marked]: #marking-text-for-redaction
[removed]: #removing-a-marked-redaction
[applied]: #applying-redactions
