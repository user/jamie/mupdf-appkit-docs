# Getting Started

<div class="banner">
  	<div class="ios-text"></div>
  	<div class="vendor-logo ios single"></div>
</div>

## System requirements

- **Xcode minimum SDK**: 9
- **iOS minimum version**: 11.0

## Adding the App Kit to your project

You should have been provided with a `mupdfdk.framework` file for your project. This file is effectively the __MuPDF App Kit__ and contains all the code required for your projects.

The linked framework should be added to your project file system and then referenced in the __Frameworks__ section in Xcode.

See the Xcode screenshot below for an example:

![Xcode frameworks](images/ios/xcode-frameworks.png)
_Xcode importing frameworks_


> **NOTE**  
> If there is an compile time error which complains that the linked framework cannot be found ensure to validate your workspace in Xcode with:
>
> `Build Settings -> Build Options -> Validate Workspace = YES`
>

## Verify your integration

To ensure you have correctly added the framework to your project ensure that your source code files can reference `import mupdfdk` without error.

## License key
To remove the MuPDF document watermark from your App, you will need to use a license key to activate App Kit.

To acquire a license key for your app you should:

1. Go to [artifex.com/appkit]
2. Purchase your license(s)
3. In your application code add the API call to activate the license

### Using your license key

If you have a license key for MuPDF App Kit, it will be bound to the App ID which you will have defined at the time of purchase. Therefore you should ensure that the App ID in your iOS project is correctly set. This is defined in the Bundle Identifier for the product `$(PRODUCT_BUNDLE_IDENTIFIER)`.



Once you have confirmed that your Bundle Identifier is correctly named, then call the following API early on in your application code:


<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">


```
import mupdfdk
...

let key:String = "put your license key here"
MuPDFDKDocumentViewController.unlockAppKit(key)
```

</div>

<div class="objc">


```
#import "mupdfdk/mupdfdk.h"
...

const char *key = "put your license key here";
[MuPDFDKDocumentViewController unlockAppKit:@(key)];
```

</div>

</div>


> **NOTE**  
> The API call to set the license key should be made before you instantiate an App Kit `DocumentView`
>

[artifex.com/appkit]: https://artifex.com/appkit
[Download the latest MuPDF App Kit]: download.html
