# PDF Table of Contents

<div class="banner">
    <div class="ios-text"></div>
    <div class="vendor-logo ios single"></div>
</div>


## Overview

Not all PDF documents will have a Table of Contents, but for those that do there will be a non-nil `toc` object associated against the `MuPDFDKDoc` document instance as follows:

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
let toc:[ARDKTocEntry]? = session.doc.toc
```

</div>

<div class="objc">



```
NSArray<id<ARDKTocEntry>> *toc = session.doc.toc;
```

</div>

</div>


> **NOTE**  
> If there is no Table of Contents for the PDF then the `toc` object will simply be `nil`.
>

To understand each `ARDKTocEntry` entry object inside the delivered array, an application developer can loop the array to strip out the entries into a flat array for listing purposes. This is because `ARDKTocEntry` items can contain sub-nested `ARDKTocEntry` items (items at lower depths representing a table of contents sub-listing from a parent item). An example of how to traverse the `toc` array is as follows:

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
func traverse(toc:[ARDKTocEntry]) {
    var tocEntries:[ARDKTocEntry] = []

    func add(entries:[ARDKTocEntry]) {
        for entry:ARDKTocEntry in entries {
            tocEntries.append(entry)
            if entry.children != nil {
                add(entries:entry.children! as! [ARDKTocEntry])
            }
        }
    }

    add(entries:toc)

    // Items should now be in a flat array, check their label and depth
    for item:ARDKTocEntry in tocEntries {
        print("item.label=\(item.label)")
        print("item.depth=\(item.depth)")
    }
}
```

</div>

<div class="objc">



```
NSMutableArray<id<ARDKTocEntry>> *tocEntries = [[NSMutableArray alloc] init];

-(void)traverse:(NSArray<id<ARDKTocEntry>> *)toc {
    [self addEntries:toc];

    // Items should now be in a flat array, check their label and depth
    for (id<ARDKTocEntry> item in self.tocEntries) {
        NSLog(@"item.label=%@",item.label);
        NSLog(@"item.depth=%lu",item.depth);
    }
}

- (void)addEntries:(NSArray<id<ARDKTocEntry>> *)entries {
    for (id<ARDKTocEntry> entry in entries) {
        [self.tocEntries addObject:entry];
        if (entry.children)
            [self addEntries:entry.children];
    }
}
```

</div>

</div>
