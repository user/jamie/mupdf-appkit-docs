# Introduction

<div class="banner intro">
	<div class="default-text"></div>
	<div class="vendor-logo mupdf"></div>
	<div class="vendor-logo android"></div>
	<div class="vendor-logo ios"></div>
</div>

Browse the latest developer documentation including API reference and sample code.


## MuPDF SDK
MuPDF is a software framework for viewing and converting PDF, XPS, and E-book documents. It builds and runs on almost any OS you can imagine.

## MuPDF App Kit
The MuPDF App Kits build upon the SDK to offer simple code samples to enable you to build mobile Apps based upon MuPDF. The developer documentation outlines how to use these App Kits for both Android and iOS platforms.

## Licensing
You are free to use the MuPDF App Kit for evaluation purposes. However, if you require to publish an application, then a commercial license is required.

For more on how to use your license key see:

[Android License Key]

[iOS License Key]

### Commercial license
A commercial license for MuPDF App Kit will be necessary before publishing your App project to an application store or other type of digital distribution platform for software applications. Please read the [Software License Terms and Conditions] document in its entirety.

<div class="flex-holder">
	<button class="cta orange" onclick="window.open('https://marketing.artifex.com/appkit#pricing', '_blank')">BUY NOW</button>
	<button class="cta orange" onclick="ArtifexShop.openDownloadPanel()">TRY FOR FREE</button>
</div>

[FSF web site]: https://www.gnu.org/licenses/agpl-3.0.html
[Software License Terms and Conditions]: https://marketing.artifex.com/appkit/terms
[Android License Key]: android-getting-started.html#license-key
[iOS License Key]: ios-getting-started.html#license-key
