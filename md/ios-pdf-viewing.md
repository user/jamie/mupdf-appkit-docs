# PDF Viewing

<div class="banner">
    <div class="ios-text"></div>
    <div class="vendor-logo ios single"></div>
</div>


## Present a document view

There are two fundamental ways of presenting a document to the screen. One way is to use the [Default UI] which includes a user interface. The alternative is to load the document into a dedicated view controller and provide your own [Custom UI] with delegate methods available for your document control.


## Default UI

The __Default UI__ is an App Kit UI created by Artifex which includes a user-interface for typical document features and actions. It is presented at the top of the document view and accommodates for both tablet and phone layout.

The __Default UI__ aims to deliver a handy way of allowing for document viewing & manipulation without the need to provide your own [Custom UI].


### Basic usage

Assuming that your view controller has a navigation controller then local documents in the iOS main bundle can be presented by pushing an instance of `ARDKDocumentViewController` from your dedicated `UIViewController` as follows:

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">


```
let documentPath:String = "sample-document.pdf"
let vc:ARDKDocumentViewController =
MuPDFDKDocumentViewController.viewController(forFilePath:documentPath, openOnPage:0)
self.navigationController?.pushViewController(vc, animated:true)
```

</div>

<div class="objc">


```
NSString *documentPath = @"sample-document.pdf";
ARDKDocumentViewController *vc =
[MuPDFDKDocumentViewController viewControllerForFilePath:documentPath openOnPage:0];
[self.navigationController pushViewController:vc animated:YES];
```

</div>

</div>


|![default-ui](images/ios/default-ui-tablet-file.png)|![default-ui](images/ios/default-ui-tablet-annotate.png)|
|--|--|
|The default UI for File operations|The default UI for Annotate operations|




### Advanced usage

Alternatively, an application developer can open a document with a session. This enables more control with regard to the settings and file operations for the document.

Opening a document within a session involves the following:

1) Initializing your own [FileState] class (ensuring to set the correct file path and document type within it):

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
let fileState:MyFileState = MyFileState()
```

</div>

<div class="objc">



```
MyFileState *fileState = [[MyFileState alloc] init];
```

</div>

</div>

2) Initializing a `MuPDFDKLib` object for use in your session:

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
let settings:ARDKSettings = ARDKSettings()
self.mupdfdkLib = MuPDFDKLib.init(settings: settings)
```

</div>

<div class="objc">



```
ARDKSettings *settings = [[ARDKSettings alloc] init];
self.mupdfdkLib = [[MuPDFDKLib alloc] initWithSettings:settings];
```

</div>

</div>

3) Initializing `ARDKDocumentSettings` with your required [configuration settings]:


<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
let docSettings:ARDKDocumentSettings = ARDKDocumentSettings()
docSettings.enableAll(true)
```

</div>

<div class="objc">



```
ARDKDocumentSettings *docSettings = [[ARDKDocumentSettings alloc] init];
[docSettings enableAll:YES];
```

</div>

</div>


4) Initializing the document session, `ARDKDocSession`,  with your `MyFileState`, `MuPDFDKLib` and `ARDKDocumentSettings` instances:


<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
let session:ARDKDocSession = ARDKDocSession(fileState:fileState,
                                              ardkLib:self.mupdfdkLib,
                                          docSettings:docSettings)
```

</div>

<div class="objc">



```
ARDKDocSession *session = [ARDKDocSession sessionForFileState:fileState
                                                      ardkLib:self.mupdfdkLib
                                                  docSettings:_docSettings];
```

</div>

</div>




5) Instantiate the `ARDKDocumentViewController` with the session


<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
let vc:ARDKDocumentViewController = MuPDFDKDocumentViewController.viewController(for: session,
                                                                          openOnPage: 0)
```

</div>

<div class="objc">



```
ARDKDocumentViewController *vc = [MuPDFDKDocumentViewController viewControllerForSession:session
                                                                              openOnPage:0];

```

</div>

</div>





### The Back button

Your dedicated view controller, which pushes the instance of `ARDKDocumentViewController`, must include one bespoke method to enable a graceful exit back from the Default UI. This method is detailed as follows:


<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">


```
@IBAction func sodk_unwindAction(_ sender: UIStoryboardSegue) {
}
```

</div>

<div class="objc">



```
- (IBAction)sodk_unwindAction:(UIStoryboardSegue *)sender {
}
```

</div>

</div>


This method is triggered by the user pressing the 'back' button in the MuPDF UI.

> **NOTE**  
> The implementation can be left empty, but this function __must__ be present in the view controller that should be unwound back to - otherwise nothing will happen when the user taps the back button.
>




### Configuration options

When using the [Default UI] an application developer can optionally set certain configurable features.


The available settings conform to the `ARDKDocumentSettings` protocol which contains the following key/value pairs:


| Key | Value type |
|---|---|
|contentDarkModeEnabled|`bool`|
|editingEnabled|`bool`|
|fullScreenModeEnabled|`bool`|
|insertFromCameraEnabled|`bool`|
|insertFromPhotosEnabled|`bool`|
|openInEnabled|`bool`|
|openUrlEnabled|`bool`|
|pdfAnnotationsEnabled|`bool`|
|pdfFormFillingAvailable|`bool`|
|pdfFormFillingEnabled|`bool`|
|pdfFormSigningEnabled|`bool`|
|pdfRedactionAvailable|`bool`|
|pdfRedactionEnabled|`bool`|
|pdfSignatureFieldCreationEnabled|`bool`|
|printingEnabled|`bool`|
|saveAsEnabled|`bool`|
|saveButtonEnabled|`bool`|
|saveToButtonEnabled|`bool`|
|securePrintingEnabled|`bool`|
|shareEnabled|`bool`|
|systemPasteboardEnabled|`bool`|

> **NOTE**  
>The configuration options should be set before a document is opened
>

To use MuPDF with configuration options an application developer should use load documents using the [document session] method.


## Custom UI

Your dedicated view controller should implement the following protocols `ARDKBasicDocViewDelegate` & `ARDKDocumentEventTarget`. For more on these protocols see [Document Listeners].

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
class DocumentViewController: UIViewController,
                              ARDKBasicDocViewDelegate,
                              ARDKDocumentEventTarget
```

</div>

<div class="objc">



```
@interface DocumentViewController() <ARDKBasicDocViewDelegate,
                                     ARDKDocumentEventTarget>
```

</div>

</div>



These protocols will allow your `UIViewController` subclass to respond to events whilst presenting & interacting with the document.

There are several ways your `UIViewController` subclass can act as a container for an instance of `MuPDFDKBasicDocumentViewController`, along with UI related views. A layout described via a storyboard is one option, or a programmatic approach, as follows:

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
let docVc:MuPDFDKBasicDocumentViewController =
MuPDFDKBasicDocumentViewController.init(forPath:documentPath)
docVc.delegate = self
docVc.session.doc.add(self)
self.addChild(docVc)
docVc.view.frame = self.view.bounds
self.view.addSubview(docVc.view)
docVc.didMove(toParent:self)
```

</div>

<div class="objc">



```
MuPDFDKBasicDocumentViewController *docVc =
[MuPDFDKBasicDocumentViewController viewControllerForPath:documentPath];
docVc.delegate = self;
[docVc.session.doc addTarget:self];
[self addChildViewController:docVc];
docVc.view.frame = self.view.bounds;
[self.view addSubview:docVc.view];
[docVc didMoveToParentViewController:self];
```

</div>

</div>



### Going to a page

Once a document is loaded an application developer can view pages either by scrolling the document view or by using the App Kit API as follows:

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
// note: page number is zero-indexed, thus this would show page 8 of your document
documentViewController.showPage(7)
```

</div>

<div class="objc">



```
// note: page number is zero-indexed, thus this would show page 8 of your document
[documentViewController showPage:7];
```

</div>

</div>


In the code sample above `documentViewController` refers to the instance of your `MuPDFDKBasicDocumentViewController`. Furthermore this API should only be called after the document has initially loaded and had it's first render (see [Document Listeners - Document completed]).


### Viewing full-screen

In order to view a document in full-screen, it is up to the application developer to hide any UI which they have present and set the frame of the document view to fill the screen. Once in full-screen, you can use [Tap selections] to exit and return from the full-screen frame to any previous UI.


[Default UI]: #default-ui
[Custom UI]: #custom-ui
[Document Listeners]: ios-document-listeners.html
[Document Listeners - Document completed]: ios-document-listeners.html#document-completed
[Tap selections]:ios-document-listeners.html#swallowing-tap-selections
[FileState]: ios-file-operations.html#filestate
[configuration settings]: #configuration-options
[document session]: #advanced-usage
