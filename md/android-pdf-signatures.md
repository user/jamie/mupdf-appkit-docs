# PDF Signatures

<div class="banner">
    <div class="android-text"></div>
    <div class="vendor-logo android single"></div>
</div>



## Overview

The details of how to implement and use the digital signature capability for PDF files can be found in the SDK’s sample application.

The directory "pdf_sign" contains example implementations of the classes required for signing. The code contains details of how each class works for an application developer to follow.

`SampleSignerFactory` is the digital signature factory, which creates instances of `SampleSigner` and `SampleVerifier`

`SampleSigner` handles the signing process.

`SampleVerifier` verifies the validity of a signature.

`SampleCertificateStore` provides access to your own X509 certificate store. This example contains three hard-coded certificates.

`SampleCertificatePicker` presents a dialog for choosing which certificate to use for signing.

`SampleCertificateViewer` presents a certificate viewer dialog, similar to the `SampleCertificatePicker` dialog, in order to show details of the certificate used for signing.

During app initialization, an application developer registers a signer factory with the SDK. This example is from the sample app:


<div class="tag sampleCode"></div>
<button class="showKotlin selected" onclick="showKotlinCode()">Kotlin</button>
<button class="showJava deselected" onclick="showJavaCode()">Java</button>
<div class="codeTabs">

<div class="kotlin selected">



```
val mSignerFactory:SampleSignerFactory? = SampleSignerFactory.getInstance()

if (mSignerFactory != null) {
    Utilities.setSigningFactoryListener(mSignerFactory)
}
```

</div>

<div class="java">



```
final SampleSignerFactory mSignerFactory = SampleSignerFactory.getInstance();

if (mSignerFactory != null) {
	Utilities.setSigningFactoryListener(mSignerFactory);
}
```

</div>

</div>


Or, you can can enable a default implementation by registering an instance of `NUIDefaultSignerFactory`.
