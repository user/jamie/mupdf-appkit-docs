# Document Lifecycle

<div class="banner">
  	<div class="ios-text"></div>
  	<div class="vendor-logo ios single"></div>
</div>


## Lifecycle


PDF documents in App Kit are always rendered inside a document view controller instance.


Depending on your integration your application should use one of the following document view controllers:

* `MuPDFDKDocumentViewController` ( for the [Default UI] )

* `MuPDFDKBasicDocumentViewController` ( for a [Custom UI] )



The [Default UI] involves the least effort and should handle all your requirements unless you need nontrivial customization, more than altering colors and icons within the UI. Simply create and present a `MuPDFDKDocumentViewController` instance. With the [Default UI], all user editing features are handled internally to the `MuPDFDKDocumentViewController` instance, without you as application programmer having to interact with the process.


For the [Custom UI], a `MuPDFDKBasicDocumentViewController` instance provides a view on the document, but with minimal UI. It is the application developer's responsibility to create additional views and menus with UI elements via which the user can perform operations on the document. An application developer would typically create a [UIViewController] subclass that acts as a container for a `MuPDFDKBasicDocumentViewController` instance, along with additional views for UI. The `UIViewController` subclass would respond to the regular view-related `UIViewController` events in the usual way, and additionally implement two further protocols to make the UI responsive to events relating to the document and its view:

* `ARDKBasicDocViewDelegate` - this allows the document view to inform us of events and request information, regarding scrolling, user taps, etc.

* `ARDKDocumentEventTarget` - this allows the document itself to inform us of events, regarding the loading of the document and selection changes etc.


## View Controller interfaces


### Pasteboard


In order to control pasteboard copy and paste information, your document view controller instance should create its own implementation of a pasteboard class following the `ARDKPasteboard` protocol.

The `ARDKPastebord` protocol is as follows:


```
@protocol ARDKPasteboard

/// Whether the pasteboard has textual content
@property(nonatomic, readonly) BOOL ARDKPasteboard_hasStrings;

/// Set/get the current contents of the clipboard
@property(nullable,nonatomic,copy,setter=ARDKPasteboard_setString:) NSString *ARDKPasteboard_string;

/// The number of times the pasteboard’s contents have changed.
@property(readonly, nonatomic) NSInteger ARDKPasteboard_changeCount;

@end
```

> **NOTE**  
> For secure applications an application may require to disable pasteboard information completely.
>

Once created any pasteboard class should be assigned to the `pasteboard` object on the document view controller.

Assuming an application developer has created their own pasteboard class as `MyPasteboard` this would be assigned as follows:

<div class="tag sampleCode"></div>
<button class="selected showSwift" onclick="showSwiftCode()">Swift</button>
<button class="deselected showObjC" onclick="showObjCCode()">Objective C</button>
<div class="codeTabs">

<div class="swift selected">



```
docVC.pasteboard = MyPasteboard()
```

</div>

<div class="objc">



```
docVC.pasteboard = [[MyPasteboard alloc] init];
```

</div>

</div>


[Default UI]: ios-pdf-viewing.html#default-ui
[Custom UI]: ios-pdf-viewing.html#custom-ui
[UIViewController]: https://developer.apple.com/documentation/uikit/uiviewcontroller
